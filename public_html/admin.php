<?php
if (isset($_REQUEST['btn_valid'])) {
    $login = $_REQUEST['f_login'];
    $password = $_REQUEST['f_password'];
}
?>

<html lang="en" manifest="manifest.manifest">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Curriculum vitae</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link href="css/overwrite.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
    </head>

    <body id="bodycolor" style="">
        <div id="fb-root"></div>
        <div class="">
            <div class="row">
                <div class="col-md-2">
                    <div class="menu">
                        <div class="logo">
                            <a href="index.html"><h1>PERSONAL</h1></a>
                            <span>stylist &amp; designer</span>
                        </div>
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <ul class="nav nav-pills nav-stacked">
                                <li role="presentation"><a class="fond" style="" href="index.html">Home</a></li>
                                <li role="presentation"><a class="fond" style="" href="admin.php">Admin</a></li>
                                <li role="presentation"><a class="fond" style="" href="settings.html">Settings</a></li>
                                <li role="presentation"><a class="fond" style="" href="about.html">About</a></li>
                                <li role="presentation"><a class="fond" style="" href="graphics.html">Graphics</a></li>
                                <li role="presentation"><a class="fond" style="" href="experience.html">Experience</a></li>
                                <li role="presentation"><a class="fond" style="" href="contact.html">Contact</a></li>
                                <li role="presentation"><a class="fond" style="" href="follow.html">Follow me</a></li>
                            </ul>
                        </div>
                        <div class="footer">
                            &copy; Personal Theme. All Right Reserved
                            <div class="credits">
                                <!--
                                  All the links in the footer should remain intact.
                                  You can delete the links only if you purchased the pro version.
                                  Licensing information: https://bootstrapmade.com/license/
                                  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Personal
                                -->
                                <a href="https://bootstrapmade.com/">Free Bootstrap Templates</a>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <form action="admin.php" method="POST">
                        <div class="form-group">
                            Login
                            <input type="text" class="form-control" name="f_login" value="">
                        </div>
                        <div class="form-group">
                            Mot de Passe
                            <input type="text" class="form-control" name="f_password" value="">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn" name="btn_valid" value="Valider">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script type="text/javascript" src="js/fliplightbox.min.js"></script>
        <script type="text/javascript" src="js/color.js"></script>
        <script type="text/javascript">
            $('#graphics').flipLightBox()
        </script>

    </body>

</html>