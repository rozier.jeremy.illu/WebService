
document.getElementById('status').innerHTML = 'Veuillez vous connecter afin d\'accéder aux paramètres de configurations.';

function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    if (response.status === 'connected') {
// CONNECTÉ SUR LE SITE AVEC FACEBOOK
        testAPI();
    } else {
// PAS CONNECTÉ

        document.getElementById('settings2').hidden = "hidden";
    }
}

// FONCTIONS SE DÉCLENCHANT LORSQUE L'UTILISATEUR TERMINER SON AUTHENTIFICATION
function checkLoginState() {

    // DECONNEXION
    FB.Event.subscribe('auth.logout', function (response) {
        document.getElementById('status').innerHTML = 'Vous vous êtes bien déconnecté de votre compte Facebook.';
    });

    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });

}

window.fbAsyncInit = function () {
    FB.init({
        appId: '189595038480426',
        cookie: true, // AUTORISE LES COOKIES SUR LA SESSION
        xfbml: true, // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.8
    });
    // ON APPELLE LA FONCTION SUIVANTE POUR DÉTERMINER SI L'UTILISATEUR EST CONNECTÉ, CONNECTÉ MAIS PAS AUTORISÉ OU PAS CONNECTÉ
    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });
};
// CHARGEMENT DU SDK FACEBOOK POUR JAVASCRIPT
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id))
        return;
    js = d.createElement(s);
    js.id = id;
    js.src = 'https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.12&appId=189595038480426&autoLogAppEvents=1';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
// MESSAGE DE BIENVENUE LORSQUE L'UTILISATEUR EST CONNECTÉ
function testAPI() {
    //deco = 1;
    document.getElementById('settings2').hidden = "";
    console.log('Salut !  Voici vos informations... ');
    FB.api('/me', function (response) {
        console.log(response);
        console.log('Actuellement connecté : ' + response.name);
        document.getElementById('status').innerHTML =
                'Bienvenue sur la partie configuration, ' + response.name + ' !';
    });
}

